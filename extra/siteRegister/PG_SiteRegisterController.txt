
/**
 * An apex class that creates a portal user
 */
public with sharing class PG_SiteRegisterController {
    // PORTAL_ACCOUNT_ID is the account on which the contact will be created on and then enabled as a portal user.
    // you need to add the account owner into the role hierarchy before this will work - please see Customer Portal Setup help for more information.       
    private static Id PORTAL_ACCOUNT_ID = '001g0000005OYzIAAW';
    
    public PG_SiteRegisterController () {
    }

    public List<PG_SupportedCountry__C> SupportedCountry;
    public List<PG_CountryLanguage__C> SupportedLanguage;
    public List<PG_SubRole__C> SubRole;
    
    public String email {get; set;}
    
    public String password {get; set {password = value == null ? value : value.trim(); } }
    public String confirmPassword {get; set { confirmPassword = value == null ? value : value.trim(); } }
    
    public String communityNickname {get; set { communityNickname = value == null ? value : value.trim(); } }
    public String firstName {get; set;}
    public String lastName {get; set;}
    public String country {get; set;}
    public String language {get; set;}
    
    public String secretQuestion {get; set;}
    public String secretAnswer {get; set;}
    
    public String role {get; set;}
    
    public List<PG_SupportedCountry__c> timeZone;
    public List<PG_countryLanguage__c> countryLanguage;
    public List<PG_CountryLanguage__c> languageCode;

    public String emailEncoding = 'ISO-8859-1';
    public String registerType = 'SelfRegister';

    private boolean isValidPassword() {
        return password == confirmPassword;
    }
    
    public PageReference registerUser() {
        // it's okay if password is null - we'll send the user a random password in that case
        if (!isValidPassword()) {
        	ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.site.passwords_dont_match);
        	ApexPages.addMessage(msg);
            return null;
        }
        system.debug(isValidPassword());
        
        communityNickname = lastName.substring(0,1) + firstName.substring(0,3);
        timeZone = [select timezone__c from PG_SupportedCountry__c where name = :Country limit 1];
        countryLanguage = [select name from PG_CountryLanguage__c where PG_SupportedCountry__r.name = :country and PG_SupportedLanguage__r.name = :language];
        languageCode = [select PG_SupportedLanguage__r.languageCode__c from PG_CountryLanguage__c where PG_SupportedLanguage__r.name = :language];
        
        User u = new User();
        u.Username = email;
        u.Email = email;
        u.CommunityNickname = communityNickname;
        u.FirstName = firstname;
        u.LastNAme = lastname;
		u.PG_SecretQuestion__c = secretQuestion;
        u.PG_SecretAnswer__c = secretAnswer;
        u.PG_RegistrationType__c = registerType;
        u.PG_SubRole__c = role;
        
        if (!timeZone.isEmpty()){
			u.TimeZoneSidKey = timeZone[0].timezone__c;
        }
        
        if (!countryLanguage.isEmpty()){
        	u.LocaleSidKey = countryLanguage[0].name;
    		u.LanguageLocaleKey = countryLanguage[0].name;
        }
        
        if (!languageCode.isEmpty()){
            u.LanguageLocaleKey = languageCode[0].PG_SupportedLanguage__r.languageCode__c;
        }
        
        u.EmailEncodingKey = emailEncoding;

        String accountId = PORTAL_ACCOUNT_ID;

        // lastName is a required field on user, but if it isn't specified, we'll default it to the username
        String userId = Site.createPortalUser(u, accountId, password);

        if (userId != null) { 
            if (password != null && password.length() > 1) {
                return Site.login(email, password, null);
            }
            else {
                PageReference page = System.Page.SiteRegisterConfirm;
                page.setRedirect(true);
                return page;
            }
        }
        return null;
    }
    
    public List<SelectOption> getItemsCountry() {
        SupportedCountry = [select Name from PG_SupportedCountry__C];
        List<SelectOption> options = new List<SelectOption>(); 

        for (PG_SupportedCountry__C item : SupportedCountry){
            options.add(new SelectOption(item.Name,item.Name));
        }
        return options;
    }
    
    public List<SelectOption> getItemsLanguage() {
        SupportedLanguage = [select PG_SupportedLanguage__r.name from PG_CountryLanguage__C]; //where PG_supportedCountry__r.name = :country];
        List<SelectOption> options = new List<SelectOption>(); 

        for (PG_CountryLanguage__C item : SupportedLanguage){
            options.add(new SelectOption(item.PG_SupportedLanguage__r.name,item.PG_SupportedLanguage__r.name));
        }
        return options;
    }
    
    public List<SelectOption> getItemsSubRole() {
        subRole = [select name from PG_SubRole__c];
        List<SelectOption> options = new List<SelectOption>(); 

        for (PG_SubRole__c item : subRole){
            options.add(new SelectOption(item.name,item.name));
        }
        return options;
    }
}